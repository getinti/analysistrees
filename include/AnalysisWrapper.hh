#ifndef AnalysisWrapper_hh
#define AnalysisWrapper_hh

#include "Global.hh"
#include "NA62Histo.hh"
#include "NA62Counter.hh"
#include "NA62Parameter.hh"
#include "K2piAnal.hh"
#include "K2piNormAnal.hh"
#include "Kmu2Anal.hh"
#include "Kmu2CaloAnal.hh"
#include "PNNAnal.hh"
#include "PMINNNAnal.hh"

#include "TProfile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TEfficiency.h"
#include "TFeldmanCousins.h"
#include "TRandom3.h"
#include "TSpline.h"
#include "TGraphAsymmErrors.h"

class AnalysisWrapper {
public:
  AnalysisWrapper();
  ~AnalysisWrapper();
  void K2PI(K2piAnal*);
  void KMU2(Kmu2Anal*);
  void KMU2CALO(Kmu2CaloAnal*);
  void NORMK2PI(K2piNormAnal*);
  void PNN(PNNAnal*);
  void PMIN(PMINNNAnal*);

protected:
  void Space() {cout<<endl;};
  void Underline() {cout << "    ----------------------------------------------  " << endl;};
  double BinomialSigma(double val, double n) { return sqrt(val*(1.-val)/n);};
  void PrintResult(const char* text, double val, double err) { cout << text << " = " << val << "+-" << err << endl; };

  void PrintMomentumBins(double,double,double);
  void PrintEfficiencyInMomentum(double,double,double,double,TEfficiency*);
  void PrintResultInMomentum(NA62Parameter*,TH1F*,TH1F*,TString,double);
  void PrintResultInMomentum(NA62Parameter*,TH1F*,TH1F*,TH1F*,TString,double);

  void PrintIntensityBins(double*,double*,int);
  void PrintEfficiencyInIntensity(int,double,TEfficiency*);
  void PrintResultInIntensity(int,TH1F*,TH1F*,TString,double);
  void PrintResultInIntensity(int,TH1F*,TH1F*,TH1F*,TString,double);

protected:
};


#endif
