#ifndef K2piAnal_hh
#define K2piAnal_hh

#include "VAnalysis.hh"

class K2piAnal : public VAnalysis {

public:
  K2piAnal();
  virtual ~K2piAnal();
  void SetParameter();
  void InitCounter();
  void ResetEvent();

  bool EventAnalysis();
  int  AnalysisKinematics();
  bool KinematicTails();
  bool CaloPID();
  bool RICHPID();

public:
  TH1F *GetHisto1D(TString name) {return (TH1F*)(fHistos.FindObject(name));};
  TH2F *GetHisto2D(TString name) {return (TH2F*)(fHistos.FindObject(name));};
protected:
  void FillHisto(TString name,Double_t x){((TH1*)(fHistos.FindObject(name)))->Fill(x);};
  void FillHisto(TString name,Double_t x,Double_t y){((TH2*)(fHistos.FindObject(name)))->Fill(x,y);};

protected:
  void     DiscriminantNormalization();
  void     SetupVariables();
  Double_t PDFKaonDT(Double_t dt);
  Double_t Discriminant(const Double_t &dt_val);
  bool     IsSnake();
  int      PionPID();
  int      GetRegion();
  double   Kmu2KinematicBound();

protected:
  THashTable fHistos; // Container of histograms

  Double_t fIntPDFKaonDT_;
  vector<double> fPDFKaonDT_;
  PnnNA62Event* fevt_;
  PnnNA62Trigger* ftrig_;
  PnnNA62DownstreamParticle* fpion_;
  PnnNA62UpstreamParticle* fkaon_;

  //Kinematic variables + instantaneous intensity
  TVector3 fvtx_;

  double fmm2_;
  double fmm2_rich_;
  double fmm2_nomi_;
  double fp_;
  double fstraw1_x_;
  double fstraw1_y_;
  double fstraw1_r_;
  double ftrim5_x_;
  double ftrim5_y_;
  double ftrim5_r_;
  double flambda_;
  double fcda_;
  bool   fpipi_region_;
  bool   fp_region_;
  int    fregion_;
  bool   fbox_cut_;

  //GTK Timing and related cuts
  double ftgtk_;
  double fdt_kk_;
  double fdt_kpi_;
  bool   fellipse_;
  double fdt13_;
  double fdt12_;
  double fdt23_;
  double fd13_;
  double fd12_;
  double fd23_;
  bool   fgtk_cut_;
  bool   fgtk_match_;

  //RICH variables
  double fmax_lh_;
  bool   frich_mass_range_;
  bool   frich_pid_;

  //Beam background variables
  bool fsnake_;
  bool fbb_cut_;

  //Photon rejection
  bool fhit_mult_;
  bool fmult_tot_;

};

#endif
