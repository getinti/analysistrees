#ifndef VAnalysis_hh
#define VAnalysis_hh

#include "Global.hh"
#include "NA62Parameter.hh"
#include "NA62Histo.hh"
#include "PnnNA62Event.hh"
#include "NA62Counter.hh"

class VAnalysis {
  public:
    VAnalysis();
    virtual ~VAnalysis();
    virtual void Process();
    virtual void SetParameter();
    virtual void InitHisto();
    virtual void InitCounter();
    virtual void Init();
    virtual void EndOfJob();
    virtual bool EventAnalysis();
    virtual void ResetEvent();
    virtual TFile *GetOutFile() {return fOutfile;};
    virtual TChain *GetTree() {return fTree;};
    virtual NA62Parameter *GetPar() {return fPar;};
    virtual NA62Histo *GetHisto() {return fHisto;};
    virtual PnnNA62Event *GetEvent() {return fEvent;};
    virtual string GetParFile() {return fParFile;};
    virtual string GetHistoList() {return fHistoList;};
    virtual string GetInputList() {return fInputList;};
    virtual string GetType() {return fType;};
    virtual NA62Counter *GetCounter() {return fCnt;};
    virtual bool GetIsMC() {return fIsMC;};
    virtual void SetOutFile(TFile *val) {fOutfile=val;};
    virtual void SetTree(TChain *val) {fTree=val;};
    virtual void SetPar(NA62Parameter *val) {fPar=val;};
    virtual void SetHisto(NA62Histo *val) {fHisto=val;};
    virtual void SetEvent(PnnNA62Event *val) {fEvent=val;};
    virtual void SetParFile(string val) {fParFile=val;};
    virtual void SetHistoList(string val) {fHistoList=val;};
    virtual void SetInputList(string val) {fInputList=val;};
    virtual void SetType(string val) {fType=val;};
    virtual void SetCounter(NA62Counter *val) {fCnt=val;};
    virtual void SetIsMC(bool val) {fIsMC=val;};

    virtual bool IsRegion(double var, double *r) {return (var>r[0]&&var<r[1]);};

  protected:
    TFile *fOutfile;
    TChain *fTree;
    NA62Parameter *fPar;
    NA62Histo *fHisto;
    PnnNA62Event *fEvent;
    string fParFile;
    string fHistoList;
    string fInputList;
    string fType;
    NA62Counter *fCnt;

  protected:
    bool fIsMC;
    double fR1[2];
    double fR2[2];
    double fP[2];
};

#endif
