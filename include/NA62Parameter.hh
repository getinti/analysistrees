#ifndef NA62Parameter_H
#define NA62Parameter_H 1

#include "Global.hh"
#include <unordered_map>

class NA62Parameter {
  public:
    NA62Parameter();
   ~NA62Parameter();
    void Set(string,double);
    double Get(string);

  protected:
    unordered_map<string,double> fParameters;
};

#endif
