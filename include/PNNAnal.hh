#ifndef PNNAnal_hh
#define PNNAnal_hh

#include "VAnalysis.hh"
#include <stdlib.h>
#include "TMVA/Reader.h"

class PNNAnal : public VAnalysis {

public:
    PNNAnal();
    virtual ~PNNAnal();
    void SetParameter();
    void InitCounter();
    void EndOfJob();
    void ResetEvent();

    bool EventAnalysis();
    int  FullPinunuAnalysis();
    int  UpstreamBackgroundAnalysis();
    int  AnalysisKinematics();
    void InelasticAnalysis();
    void SnakesAnalysis();

public:
    TH1F *GetHisto1D(TString name) {return (TH1F*)(fHistos.FindObject(name));};
    TH2F *GetHisto2D(TString name) {return (TH2F*)(fHistos.FindObject(name));};
protected:
    void FillHisto(TString name,Double_t x){((TH1*)(fHistos.FindObject(name)))->Fill(x);};
    void FillHisto(TString name,Double_t x,Double_t y){((TH2*)(fHistos.FindObject(name)))->Fill(x,y);};

protected:
    //Tools and initfunctions
    TVector3 GetZ(TVector3 theta,TVector3 initpos, Double_t zpos);
    void DiscriminantNormalization();
    void SetupVariables();
    int  GetRegion(bool print);
    int  GetCategory();
    Double_t PDFKaonDT(Double_t dt);
    Double_t Discriminant(const Double_t &dt_val);
    bool     IsSnake();
    int      PionPID() ;
    double   Kmu2KinematicBound();
protected:
    double fPStep;      // Size of momentum bins
    int fIBins;         // Number of intensity bins
    double *fIMin;      // Bin minimum / bins of intensity
    double *fIMax;      // Bin maximum / bins of intensity
    THashTable fHistos; // Container of histograms

    Double_t fIntPDFKaonDT_;
    vector<double> fPDFKaonDT_;
    PnnNA62Event* fevt_;
    PnnNA62Trigger* ftrig_;
    PnnNA62DownstreamParticle* fpion_;
    PnnNA62UpstreamParticle* fkaon_;

    //Kinematic variables + instantaneous intensity
    TVector3 fvtx_;

    double fmm2_;
    double fmm2_rich_;
    double fmm2_nomi_;
    double fp_;
    double fstraw1_x_;
    double fstraw1_y_;
    double fstraw1_r_;
    double ftrim5_x_;
    double ftrim5_y_;
    double ftrim5_r_;
    double flambda_;
    double fcda_;
    double fthx_;
    double fthy_;
    bool   fpipi_region_;
    int    fregion_;
    bool   fbox_cut_;

    //GTK Timing and related cuts
    double fdt_kk_;
    double fdt_kpi_;
    bool   fgtk_match_;

    //PID variables
    double fmax_lh_;
    bool   frich_mass_range_;
    bool   frich_pid_;
    bool   fcalo_pid_;

    //Beam background variables
    bool fsnake_;
    bool fbb_cut_;
    bool fbb_cut_BDTTrees;

    //Photon rejection
    bool fphoton_;
    bool fhit_mult_;
    bool fmult_tot_;
    bool fmult_tot_BDTTrees;

    //Categories
    int fNCategories;    // Number of categories

    vector<bool> fpid_;
    vector<bool> fk_pi_match_;
    vector<bool> fpi0_rej_;
    vector<bool> fkin_cuts_;

  // BDT training
  TTree* fTreeSig;
  TTree* fTreeBkg;

  Bool_t fFlagFillBDTTrees;
  Bool_t fFlagReaderBDT;

  Bool_t fFlagSelectionForBDT;

  Bool_t fIsSignalRegions;
  Bool_t fIsInTime;

  Int_t fRunNumber;
  Int_t fBurstNumber;
  Bool_t fIsMC;
  Bool_t fBlindCut;

  // BTD reader
  TMVA::Reader *fTMVAReader;
  float fTMVAVariables[10];
  Double_t fTMVAValue;
  Double_t fTMVACutValue;
  Bool_t fTMVAUpstreamCut;

};

#endif
