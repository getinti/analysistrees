#ifndef NA62Histo_H
#define NA62Histo_H 1

#include "TROOT.h"
#include "TH1F.h"
#include "TH2F.h"
#include "THashTable.h"
#include "Global.hh"

class NA62Histo 
{

public:
  NA62Histo();

public:
  void Book(TString);   
  void FillHisto(TString name,Double_t x){((TH1*)(fHistos.FindObject(name)))->Fill(x);};
  void FillHisto(TString name,Double_t x,Double_t y){((TH2*)(fHistos.FindObject(name)))->Fill(x,y);};
  void FillHisto(TString name,Double_t x,Double_t y,Double_t w){((TH2*)(fHistos.FindObject(name)))->Fill(x,y,w);};
  Int_t GetBinId(TString name,Double_t x,Double_t y){return ((TH2*)(fHistos.FindObject(name)))->FindBin(x,y);};
  Double_t GetBinContent(TString name,Int_t ibin){return ((TH2*)(fHistos.FindObject(name)))->GetBinContent(ibin);};
  void SetBinContent(TString name,Int_t ibin,Double_t w){((TH2*)(fHistos.FindObject(name)))->SetBinContent(ibin,w);};
  void Reset(TString name){((TH2*)(fHistos.FindObject(name)))->Reset();};
  void Write();

private:
  THashTable fHistos;
};
#endif

