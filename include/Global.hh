#ifndef Global_hh 
#define Global_hh 

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH2.h"
#include "TRandom.h"
#include "TClassTable.h"
#include "TSystem.h"
#include "TROOT.h"
#include "Riostream.h"
#include "TMath.h"
#include "TLorentzVector.h"
using namespace std;

#endif
