#ifndef NA62Counter_H
#define NA62Counter_H 1

#include "Global.hh"
#include <unordered_map>

class NA62Counter {
  public:
    NA62Counter();
   ~NA62Counter();
    void Set(string);
    void Add(string);
    int Get(string);

  protected:
    unordered_map<string,int> fCounters;
};

#endif
