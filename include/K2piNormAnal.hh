#ifndef K2piNormAnal_hh
#define K2piNormAnal_hh

#include "VAnalysis.hh"
#include <stdlib.h>
#include "TMVA/Reader.h"

class K2piNormAnal : public VAnalysis {

public:
    K2piNormAnal();
    virtual ~K2piNormAnal();
    void SetParameter();
    void InitCounter();
    void ResetEvent();
    bool EventAnalysis();

    void TriggerAnalysis();

protected:
    //Tools and init function
    void     DiscriminantNormalization();
    void     SetupVariables();
    bool     IsSnake();
    Double_t PDFKaonDT(const Double_t& dt);
    Double_t Discriminant(const Double_t &dt_val);
    Int_t    GetRegion() ;
    Double_t Kmu2KinematicBound();
  int PionPID();
public:
    TH1F *GetHisto1D(const TString &name) {return (TH1F*)(fHistos.FindObject(name));};
    TH2F *GetHisto2D(const TString &name) {return (TH2F*)(fHistos.FindObject(name));};
protected:
    void FillHisto(const TString &name,const Double_t &x){((TH1*)(fHistos.FindObject(name)))->Fill(x);};
    void FillHisto(const TString &name,const Double_t &x,const Double_t &y){((TH2*)(fHistos.FindObject(name)))->Fill(x,y);};

protected:
    THashTable fHistos; // Container of histograms

    Double_t                   fIntPDFKaonDT_;
    vector<double>             fPDFKaonDT_;
    PnnNA62Event*              fevt_;
    PnnNA62Trigger*            ftrig_;
    PnnNA62DownstreamParticle* fpion_;
    PnnNA62UpstreamParticle*   fkaon_;

    //Kinematic variables + instantaneous intensity
    TVector3 fvtx_;

    double fmm2_;
    double fmm2_rich_;
    double fmm2_nomi_;
    double fp_;
    double fstraw1_x_;
    double fstraw1_y_;
    double fstraw1_r_;
    double ftrim5_x_;
    double ftrim5_y_;
    double ftrim5_r_;
    double flambda_;
    double fcda_;
    bool   fpipi_region_;
    bool   fp_region_;
    int    fregion_;
    bool   fbox_cut_;
  double fthx_;
  double fthy_;

    //GTK matching
    double fdt_kk_;
    double fdt_kpi_;
    bool   fgtk_match_;

    //RICH variables
    double fmax_lh_;
    bool   frich_mass_range_;
    bool   frich_pid_;

  bool   fcalo_pid_;

    //Beam background variables
    bool fsnake_;
    bool fbb_cut_;

    //Photon rejection
    bool fphoton_;
    bool fhit_mult_;
    bool fmult_tot_;

  Bool_t fFlagReaderBDT;
  Bool_t fFlagSelectionForBDT;
  Bool_t fIsNormRegion;
  Bool_t fIsInTime;

  bool fbb_cut_BDTTrees;
  bool fmult_tot_BDTTrees;

  Int_t fRunNumber;
  Bool_t fIsMC;
  Bool_t fBlindCut;
  
  // BTD reader
  TMVA::Reader *fTMVAReader;
  float fTMVAVariables[10];
  Double_t fTMVAValue;
  Double_t fTMVACutValue;
  Bool_t fTMVAUpstreamCut;

};

#endif
