#include "K2piAnal.hh"
#include "K2piNormAnal.hh"
#include "Kmu2Anal.hh"
#include "Kmu2CaloAnal.hh"
#include "PNNAnal.hh"
#include "PMINNNAnal.hh"
#include "AnalysisWrapper.hh"

int main(int argc, char **argv)
{
  string list = "dummy";
  string parfile;
  string outputfile = "output.root";
  string configfile = "ConfFile.txt";
  string histolist[20];
  string histodir[20];
  string type[20];
  VAnalysis *analysis[20];
  for (int kj=0; kj<20; kj++) analysis[kj]=0;

  int janalysis = 0;

  // Read input
  int i;
  for (i=1;i<argc;i++)
  {
    if (!strcmp(argv[i],"-l")) list = argv[++i];
    if (!strcmp(argv[i],"-o")) outputfile = argv[++i];
    if (!strcmp(argv[i],"-c")) configfile = argv[++i];
  }

  // Read Configuration file
  TString line;
  ifstream conf(configfile);
  while(line.ReadLine(conf)) {
    if (line.BeginsWith("#")) continue;
    TObjArray * l = line.Tokenize(" ");
    if (line.BeginsWith("InputFile=")) {
      list = ((TObjString*)(l->At(1)))->GetString();
      l->Delete();
    }
    if (line.BeginsWith("OutputFile=")) {
      outputfile = ((TObjString*)(l->At(1)))->GetString();
      l->Delete();
    }
    if (line.BeginsWith("ParameterFile=")) {
      parfile = ((TObjString*)(l->At(1)))->GetString();
      l->Delete();
    }
    if (line.BeginsWith("Analysis=")) {
      histolist[janalysis] = ((TObjString*)(l->At(1)))->GetString();
      histodir[janalysis] = ((TObjString*)(l->At(2)))->GetString();
      type[janalysis] = ((TObjString*)(l->At(3)))->GetString();
      if (type[janalysis]=="TK2PI"    ) analysis[janalysis] = new K2piAnal();
      if (type[janalysis]=="TKMU2"    ) analysis[janalysis] = new Kmu2Anal();
      if (type[janalysis]=="TKMU2CALO") analysis[janalysis] = new Kmu2CaloAnal();
      if (type[janalysis]=="TNORMK2PI") {
        analysis[janalysis] = new K2piNormAnal();
      }
      if (type[janalysis]=="TPNN"     ) {
        analysis[janalysis] = new PNNAnal();
      }
      if (type[janalysis]=="TPNN_MIN"  ) analysis[janalysis] = new PMINNNAnal();
      janalysis++;
      l->Delete();
    }
  }
  conf.close();

  // ERROR
  if (list=="dummy")
  {
    cout << "ERROR: no input files !!!!" << endl;
    cout << "No more actions: EXIT PROGRAM" << endl;
    return 0;
  }

  // Open output file
  TFile *outfile = new TFile(outputfile.c_str(),"RECREATE");
  for (int kj=0; kj<janalysis; kj++) outfile->mkdir(histodir[kj].c_str());

  // Start
  cout << "*******************" << endl;
  cout << "Analysis started..." << endl;
  cout << "*******************" << endl;
  cout << "File " << list << " opened" << endl;
  cout << endl;

  // Run Analysis
  for (int kj=0; kj<janalysis; kj++) {
    if (!analysis[kj]) {
      cout << "WARNING: tree " << type[kj] << " not present in the input file !!!!" << endl;
      continue;
    }
    cout << list << "type - " << type[kj] << "histolist - " << histolist[kj] << endl;
    outfile->cd(histodir[kj].c_str());
    analysis[kj]->SetInputList(list);
    analysis[kj]->SetParFile(parfile);
    analysis[kj]->SetType(type[kj]);
    analysis[kj]->SetHistoList(histolist[kj]);
    analysis[kj]->Process();
    outfile->cd("..");
    cout << endl;
  }

  AnalysisWrapper *endanalysis = new AnalysisWrapper();
  for (int kj=0; kj<janalysis; kj++) {
    outfile->cd(histodir[kj].c_str());
    if (type[kj]=="TK2PI" || type[kj]=="TK2PI") {
      endanalysis->K2PI((K2piAnal *)analysis[kj]);
    }
    if (type[kj]=="TKMU2" || type[kj]=="TKMU2") {
      endanalysis->KMU2((Kmu2Anal *)analysis[kj]);
    }
    if (type[kj]=="TKMU2CALO"){
      endanalysis->KMU2CALO((Kmu2CaloAnal *)analysis[kj]);
    }
    if (type[kj]=="TNORMK2PI" || type[kj]=="TNORMK2PI") {
      endanalysis->NORMK2PI((K2piNormAnal *)analysis[kj]);
    }
    if (type[kj]=="Pnn" || type[kj]=="TPNN") {
      endanalysis->PNN((PNNAnal *)analysis[kj]);
    }
    if (type[kj]=="PMIN" || type[kj]=="TPNN_MIN") {
      endanalysis->PMIN((PMINNNAnal *)analysis[kj]);
    }
    outfile->cd("..");
  }

  // End
  cout << "*********************" << endl;
  cout << "... Analysis finished" << endl;
  cout << "*********************" << endl;
  cout << "Ouptut written in " << outputfile << endl;
  cout << endl;

  // Close output file
  outfile->Close();

  return 0;
}
