#include "Kmu2CaloAnal.hh"

Kmu2CaloAnal::Kmu2CaloAnal(){
}
Kmu2CaloAnal::~Kmu2CaloAnal(){

}

void Kmu2CaloAnal::SetParameter() {
}
void Kmu2CaloAnal::InitCounter() {
}
void Kmu2CaloAnal::SetupVariables() {

    ResetEvent();

    fevt_  = GetEvent();
    fmuon_ = (PnnNA62DownstreamParticle *)fevt_->GetDownstreamParticle();
    fkaon_ = (PnnNA62UpstreamParticle *)fevt_->GetUpstreamParticle();
    ftrig_ = (PnnNA62Trigger *)fevt_->GetTrigger();

    //Kinematic variables + instantaneous intensity
    fvtx_      = fevt_->GetVertex();
    fmm2_      = fevt_->GetMmuMiss();
    fmm2_rich_ = fevt_->GetMMissRich();
    fmm2_nomi_ = fevt_->GetMMissNominalK();
    fp_        = fmuon_->GetP().Mag();
    ftrim5_x_  = fmuon_->GetPosTrim5X();
    ftrim5_y_  = fmuon_->GetPosTrim5Y();
    ftrim5_r_  = sqrt(pow(ftrim5_x_,2) + pow(ftrim5_y_, 2));
    fstraw1_x_ = fmuon_->GetPosStraw1X();
    fstraw1_y_ = fmuon_->GetPosStraw1Y();
    fstraw1_r_ = sqrt( pow(fstraw1_x_ - 101.2, 2) + pow(fstraw1_y_, 2));
    flambda_   = fevt_->GetLambda();
    fcda_      = fevt_->GetCDA();
    fbox_cut_  = fabs(ftrim5_x_)<100. && fabs(ftrim5_y_)<500.;

    //GTK timing variables and related cuts
    //if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.005 && fevt_->GetIsRICHBeamDiscriminant() > 0.005)
    if ((fevt_->GetIsKTAGBeamDiscriminant()>0.03 || fevt_->GetIsRICHBeamDiscriminant()>0.03) && fevt_->GetIsKTAGBeamDiscriminant() > 0.01 && fevt_->GetIsRICHBeamDiscriminant() > 0.01) // new working point for the discriminants
        fgtk_match_ = true;

    ftgtk_     = (fkaon_->GetTGTK1()+fkaon_->GetTGTK2()+fkaon_->GetTGTK3())/3.;
    fdt_       = ftgtk_-fkaon_->GetTKTAG();
    fdt13_     = fkaon_->GetTGTK1() - fkaon_->GetTGTK3();
    fdt23_     = fkaon_->GetTGTK2() - fkaon_->GetTGTK3();
    fdt12_     = fkaon_->GetTGTK1() - fkaon_->GetTGTK2();

    //RICH variables
    for (int j=0; j<5; j++) {
        if (j==3) continue;
        if (fmuon_->GetRICHLikeProb(j)> fmax_lh_) fmax_lh_ = fmuon_->GetRICHLikeProb(j);
    }
    // frich_mass_range_ = fmuon_->GetRICHMass()>0.118&&fmuon_->GetRICHMass()<0.2 ? true : false;
    // if (fmax_lh_<=0.05 && frich_mass_range_) frich_pid_ = true;
    frich_mass_range_ = fmuon_->GetRICHMass()>0.125&&fmuon_->GetRICHMass()<0.2 ? true : false;
    if (fmax_lh_<=0.12 && frich_mass_range_) frich_pid_ = true;

    fcalo_pid_    = PionPID();

    //Beam background variables
    fsnake_       = IsSnake();
    fbb_cut_      = fsnake_ || fevt_->GetIsSignalInCHANTI() || fevt_->GetIsInteraction() || fkaon_->GetNGTKTracks()>5 || fvtx_.Z() <= 105000 || fvtx_.Z() > 165000;

    //Region definitions
    fpipi_region_ = fmm2_ <= 0.021 && fmm2_ > 0.015;
    fp_region_    = fp_ <= 35 && fp_ > 15;

    //PhotonRejection
    fphoton_      = fevt_->GetIsSignalInLAV() || fevt_->GetIsSignalInSAC() || fevt_->GetIsSignalInIRC() || fevt_->GetIsSignalInSAV() || fevt_->GetIsPhotonInLKr() || fevt_->GetIsAuxiliaryLKr();
    fhit_mult_    = fevt_->GetMultAllOldCH7() > 3 ||fevt_->GetMultOldNewCH() > 0 || fevt_->GetMultNewCHLKr() > 0 || fevt_->GetMultOldCHLKr() > 0 || fevt_->GetMultHAC() == 1 || fevt_->GetMultMUV0() == 1 || (fevt_->GetMultLKrMerged() && fevt_->GetMultAllOldCH7() > 0);
    fmult_tot_    = fevt_->GetIsMultiplicity() || fevt_->GetIsSegment() || !fevt_->GetIsOneParticle() || fevt_->GetIsBroadMultiVertex();

    return;
}
void Kmu2CaloAnal::ResetEvent() {

    fevt_  = NULL;
    fmuon_ = NULL;
    fkaon_ = NULL;

    fvtx_      = TVector3(0.,0.,0.);
    fmm2_      = -99999;
    fmm2_rich_ = -99999;
    fmm2_nomi_ = -99999;
    fp_        = -99999;
    ftrim5_x_  = -99999;
    ftrim5_y_  = -99999;
    ftrim5_r_  = -99999;
    flambda_   = -99999;
    fcda_      = -99999;

    fgtk_match_ = 0.;
    ftgtk_     = -99999;
    fdt_       = -99999;
    fdt13_     = -99999;
    fdt23_     = -99999;
    fdt12_     = -99999;

    fcalo_pid_ = 0;
    frich_pid_ = 0;
    fmax_lh_   = -99999;
    frich_mass_range_ = 0;
    fsnake_    = 0;
    fpipi_region_ = 0;
    fp_region_ = 0;
    fbb_cut_   = 0;

    fphoton_   = 0;
    fhit_mult_ = 0;
    fmult_tot_ = 0;
    fbox_cut_  = 0;
}

bool Kmu2CaloAnal::EventAnalysis() {

    SetupVariables();
    if (fevt_->GetTriggerMask()==1) return 0;

    // BeamBackgroundCut
    if(fbb_cut_) return 0;
    if(fevt_->GetIsBroadMultiVertex()) return 0;
    //if(!fp_region_) return 0;
    //if((int)fevt_->GetTimestamp()% 256 < 35 && (int)fevt_->GetTimestamp()%256 > 28) return 0;



    fHisto->FillHisto("lambda", flambda_);
    fHisto->FillHisto("mmiss_vs_p", fp_, fmm2_);
    fHisto->FillHisto("mmiss_vs_mmissRICH", fmm2_, fmm2_rich_);
    fHisto->FillHisto("mmiss_vs_mmissNominalK", fmm2_, fmm2_nomi_);

    CaloPID();
    RICHPID();

    return 0;
}

bool Kmu2CaloAnal::CaloPID(){

    if(fmuon_->GetRICHLikeMost()!=2) return 0;
    if(fabs(fdt_) > 0.6) return 0;

    fHisto->FillHisto("CaloPID_mmiss_vs_p_0", fp_, fmm2_);
    if(!PionPID()) return 0;
    fHisto->FillHisto("CaloPID_mmiss_vs_p_1", fp_, fmm2_);


}
bool Kmu2CaloAnal::RICHPID(){
    if(fabs(fdt_) > 0.6) return 0;
    if(fmuon_->GetIsMuon()!=3) return 0;
    if(fmuon_->GetCaloMuonProb() <= 0.99) return 0;

    fHisto->FillHisto("RICHPID_mmiss_vs_p_0", fp_, fmm2_);
    if(fmuon_->GetIsRICHSingleRing())
        fHisto->FillHisto("RICHPID_mmiss_vs_p_00", fp_, fmm2_);
    if(frich_mass_range_)
        fHisto->FillHisto("RICHPID_mmiss_vs_p_01", fp_, fmm2_);
    // if(fmax_lh_<=0.05)
    if(fmax_lh_<=0.12)
        fHisto->FillHisto("RICHPID_mmiss_vs_p_02", fp_, fmm2_);

    if(!frich_pid_) return 0;
    fHisto->FillHisto("RICHPID_mmiss_vs_p_1", fp_, fmm2_);



}
int Kmu2CaloAnal::PionPID() {

    bool D = 0;

    if (fmuon_->GetIsMUV3()) D = 1;
    if (fmuon_->GetIsMulti()) D = 1;
    if (fmuon_->GetIsMip()) D = 1;

    Double_t caloEnergy = fmuon_->GetCaloEnergy();
    Double_t r1 = fmuon_->GetMUV1Energy()/caloEnergy;
    Double_t r2 = fmuon_->GetMUV2Energy()/caloEnergy;
    Double_t elkr = fmuon_->GetLKrEnergy();
    Double_t seedratio = fmuon_->GetLKrSeedEnergy()/elkr;
    Double_t cellratio = fmuon_->GetLKrNCells()/elkr;
    bool isLKrEM = 0;
    auto vcut       = TMath::Max(0.7,0.98-0.4596*exp(-(fp_-11.44)/5.27));
    bool IsCaloPion = fmuon_->GetCaloPionProb()>=vcut;

    if (r1<0.01 && r2<0.01) {
        if (seedratio>0.2&&cellratio<=3) isLKrEM = 1;
        if (seedratio<=0.2&&cellratio<1.8) isLKrEM = 1;
        if (seedratio>0.35) isLKrEM = 1;
        if (seedratio<0.05) isLKrEM = 1;
    }
    if (seedratio>0. && seedratio<0.8 && cellratio<1.4) isLKrEM = 1;

    if(!IsCaloPion) D = 1;
    if (isLKrEM) D = 1;
    if (caloEnergy>1.2*fp_) D = 1;
    if (elkr>0.8*fp_) D = 1;
    if (!fmuon_->GetIsGoodMUV1() && fmuon_->GetIsGoodMUV2()) D = 1;

    return D > 0 ? 0 : 1;
}

bool Kmu2CaloAnal::IsSnake() {
    PnnNA62Event *evt = GetEvent();
    PnnNA62DownstreamParticle *pion = (PnnNA62DownstreamParticle *)evt->GetDownstreamParticle();
    double zvtx = evt->GetVertex().Z();
    // double rst1 = sqrt((pion->GetPosStraw1().X()-101.2)*(pion->GetPosStraw1().X()-101.2)+pion->GetPosStraw1().Y()*pion->GetPosStraw1().Y());
    double rst1 = sqrt((pion->GetPosStraw1X()-101.2)*(pion->GetPosStraw1X()-101.2)+pion->GetPosStraw1Y()*pion->GetPosStraw1Y());
    double b1 = -0.004;
    double a1 = 315-b1*105000.;
    double r115 = a1+b1*115000.;
    double b2 = -(900-r115)/10000.;
    double a2 = 900-b2*105000.;
    double b3 = -0.00983333333;
    double a3 = 780-b3*105000.;
    double cut1 = a1+b1*zvtx;
    double cut2 = a2+b2*zvtx;
    double cut3 = a3+b3*zvtx;
    bool isSignal = rst1>cut1 && rst1>cut2 && rst1<cut3 && (zvtx)<165000.;
    return !isSignal;
}
