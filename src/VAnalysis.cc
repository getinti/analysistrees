#include "VAnalysis.hh"

VAnalysis::VAnalysis() { }
VAnalysis::~VAnalysis() { }

void VAnalysis::SetParameter() {
  fPar = new NA62Parameter();
  ifstream parfile(fParFile.c_str(),ios::in);
  TString line;
  while(line.ReadLine(parfile)) {
    if (line.BeginsWith("#")) continue;
    TObjArray * l = line.Tokenize(" ");
    string namepar = ((TObjString*)(l->At(0)))->GetString().Data();
    double val = ((TObjString*)(l->At(1)))->GetString().Atof();
    fPar->Set(namepar,val);
    l->Delete();
  }
  parfile.close();
  fIsMC=fPar->Get("DataType");
  fR1[0]=fPar->Get("mr1inf");
  fR1[1]=fPar->Get("mr1sup");
  fR2[0]=fPar->Get("mr2inf");
  fR2[1]=fPar->Get("mr2sup");
  fP[0]=fPar->Get("pmin");
  fP[1]=fPar->Get("pmax");
}

void VAnalysis::InitHisto() {
  fHisto = new NA62Histo();
  fHisto->Book(fHistoList.c_str());
}

void VAnalysis::InitCounter() {
  fCnt = new NA62Counter();
}

void VAnalysis::Init() {
  fTree = new TChain(fType.c_str(),"");
  ifstream filelist(fInputList.c_str(),ios::in);
  while(!filelist.eof()) {
    string name;
    filelist >> name;
    if (filelist.eof()) break;
    fTree->AddFile(name.c_str());
    cout << "-> File " << name.c_str() << " added" << endl;
  }
  filelist.close();
  fEvent = new PnnNA62Event();
  fEvent->Clear();
  fTree->SetBranchAddress("NA62Event",&fEvent);
}

void VAnalysis::Process() {
  cout << " ###### " << fType << " ###### " << endl;
  SetParameter();
  InitHisto();
  InitCounter();
  Init();
  Long64_t nEvents = (Long64_t)GetTree()->GetEntries();
  cout << "Total number of events: " << nEvents << endl;
  for (Long64_t jevent=0;jevent<nEvents;jevent++) {
      GetTree()->GetEntry(jevent);
      EventAnalysis();
      if (jevent && fmod((Double_t)jevent,100000.)==0.) cout << "Event Processed " << jevent << endl;
  }
  fHisto->Write();
  EndOfJob();
}

bool VAnalysis::EventAnalysis() { return 1; }
void VAnalysis::ResetEvent() { }
void VAnalysis::EndOfJob(){}
